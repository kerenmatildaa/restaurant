const menu = [
  {
      id: 1,
      gambar: "img/burger1.png",
      nama_menu: "Chicken Muffin with Egg",
      desk_menu: "Setangkup English muffin hangat dilapisi dengan saus mayonais, daging ayam olahan yang digoreng sempurna, telur, dan keju.",
      harga: "Rp.30,000",
    },
    {
      id: 2,
      gambar: "img/burger2.png", 
      nama_menu: "Spicy Yakiniku Burger",
      desk_menu: "Burger daging paha ayam pedas dengan nori (rumput laut) dan irisan kol, yang dilengkapi saus yakiniku dan saus Chicken yang disajikan dalam setangkup roti lembut.",
      harga: "Rp.44,000",
  },
  {
    id: 3,
    gambar: "img/rice1.png", 
    nama_menu: "Honey Garlic Chicken Rice",
    desk_menu: "Nasi hangat dengan topping daging ayam disajikan dengan saus honey garlic.",
    harga: "Rp.16,000",
  },
  {
    id: 4,
    gambar: "img/kentang.jpg", 
    nama_menu: "French Fries",
    desk_menu: "Kentang goreng yang renyah dan gurih dengan tambahan bumbu.",
    harga: "Rp.15,000",
  },
  {
    id: 5,
    gambar: "img/regpiz.png", 
    nama_menu: "Spicy Italino Regular Pizza",
    desk_menu: "Chicken Pepperoni dan Sausage dengan Mozzarella di Spicy Italian base sauce.",
    harga: "Rp.70,000",
  },
  {
    id: 6,
    gambar: "img/cheeseroll.png", 
    nama_menu: "Cheese Roll",
    desk_menu: "Keju mozzarella gurih di dalam roti empuk dengan taburan rempah khas Italia",
    harga: "Rp.30,000",
  },
  {
    id: 7,
    gambar: "img/sprite.png", 
    nama_menu: "Sprite",
    desk_menu: "Sprite dingin. (Bisa pakai es batu).",
    harga: "Rp.10,000",
  },
  {
    id: 8,
    gambar: "img/cola.png", 
    nama_menu: "Coca-Cola",
    desk_menu: "Coca-cola dingin. (Bisa pakai es batu).",
    harga: "Rp.10,000",
  },
  {
    id: 9,
    gambar: "img/fanta.png", 
    nama_menu: "Fanta",
    desk_menu: "Fanta Strawberry dingin. (Bisa pakai es batu).",
    harga: "Rp.10,000",
  },
  {
    id: 10,
    gambar: "img/blueocean.png", 
    nama_menu: "Blue Ocean",
    desk_menu: "Sirup bluberi, lemon, biji selasih, potongan jeruk nipis.",
    harga: "Rp.31,000",
  },
  {
    id: 11,
    gambar: "img/tofcof.png", 
    nama_menu: "Tofee Cofee",
    desk_menu: "Kopi, Sirup Karamel dan Whipped Cream.",
    harga: "Rp.31,000",
  },
  {
    id: 12,
    gambar: "img/aqua.png", 
    nama_menu: "Aqua",
    desk_menu: "Air mineral aqua. (Bisa pakai es batu).",
    harga: "Rp.31,000",
  },
]; 

//Map

const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}


const callbackMap = (item, index)=>{
  const element = document.querySelector('#menu');

  element.innerHTML += `<div class="col col-md-3 mb-3 mt-3">
                          <div class="card">
                            <img src="${item.gambar}" class="card-img-top" height="250px">
                            <div class="card-body">
                              <h5 class="card-title">${item.nama_menu}</h5>
                              <p class="card-text">${item.desk_menu}</p>
                              <p class="font-weight-bold">${item.harga}</p>
                              <a href="#" class="btn btn-outline-dark my-2 my-sm-0">Beli</a>
                            </div>
                          </div>
                       </div>`
}

menu.map(callbackMap);
jumlahMENU(menu);

//Filter

const buttonElmnt = document.querySelector('.button-cari');
buttonElmnt.addEventListener('click', ()=>{
    const hasilPencarian = menu.filter((item, index)=>{
                      const inputElmnt  = document.querySelector('.input-keyword');   
                      const namaItem    = item.nama_menu.toLowerCase();
                      const keyword     = inputElmnt.value.toLowerCase();

                      return namaItem.includes(keyword);
                      })

    document.querySelector('#menu').innerHTML = '';
    hasilPencarian.map(callbackMap);
    jumlahMENU(hasilPencarian);
})


