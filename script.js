const menuMakanan = [
	{
		fotoMakanan: "img/burger2.png", 
		namaMakanan: "Spicy Yakiniku Burger",
		deskMakanan: "Burger daging paha ayam pedas dengan nori (rumput laut) dan irisan kol, yang dilengkapi saus yakiniku dan saus Chicken yang disajikan dalam setangkup roti lembut.",
		hargaMakanan: "Rp.44,000"
	}, {
		fotoMakanan: "img/rice1.png", 
		namaMakanan: "Honey Garlic Chicken Rice",
		deskMakanan: "Nasi hangat dengan topping daging ayam disajikan dengan saus honey garlic.",
		hargaMakanan: "Rp.16,000"
	}, {
		fotoMakanan: "img/kentang.jpg", 
		namaMakanan: "French Fries",
		deskMakanan: "Kentang goreng yang renyah dan gurih dengan tambahan bumbu.",
		hargaMakanan: "Rp.15,000"
	}, {
		fotoMakanan: "img/regpiz.png", 
		namaMakanan: "Spicy Italino Regular Pizza",
		deskMakanan: "Chicken Pepperoni dan Sausage dengan Mozzarella di Spicy Italian base sauce.",
		hargaMakanan: "Rp.70,000"
	}, {
		fotoMakanan: "img/cheeseroll.png", 
		namaMakanan: "Cheese Roll",
		deskMakanan: "Keju mozzarella gurih di dalam roti empuk dengan taburan rempah khas Italia",
		hargaMakanan: "Rp.30,000"
	}
];

const body = document.querySelector(".box-makanan");

for(let data of menuMakanan){
	box-makanan.innerHTML += `<div class="box-makanan">
      <img src="${data.fotoMakanan}">
      <h5>${data.namaMakanan}</h5>
      <p>${data.deskMakanan}</p>
      <p style="font-weight: bold;">${data.hargaMakanan}</p>
      <a href="#">Beli</a>
    </div>`;
}

<form class="form-inline my-2 my-lg-0" style="color: black;">
		      <input class="input-keyword" type="search" placeholder="Search" aria-label="Search" class="form-control mr-sm-2">
		      <button class="btn btn-outline-dark my-2 my-sm-0, button-cari" type="submit" style="color: black;">Search</button>
		    </form>

  menu.map((item, index)=>{
  const element = document.querySelector('#menu');

  element.innerHTML += `<div class="col col-md-3 mb-3 mt-3">
                          <div class="card">
                            <img src="${item.gambar}" class="card-img-top" height="250px">
                            <div class="card-body">
                              <h5 class="card-title">${item.nama_menu}</h5>
                              <p class="card-text">${item.desk_menu}</p>
                              <p class="font-weight-bold">${item.harga}</p>
                              <a href="#" class="btn btn-outline-dark my-2 my-sm-0">Beli</a>
                            </div>
                          </div>
                       </div>`
});

  const jumlahPesanan      = []
const buttonPesan        = document.querySelector('.btn-outline-dark my-2 my-sm-0');
const elmntJumlahPesanan = document.querySelector('.jumlahPesanan > p');

buttonPesan.forEach((item, sindex)=>{
item.addEventListener('click', ()=>{
  jumlahPesanan.push(1);

  const hasil = jumlahPesanan.reduce((accumulator, currentValue)=>{
    return accumulator + currentValue;
  }, 0);

  elmntJumlahPesanan.innerHTML = hasil;
  })
})

const jumlahMenu     = (array) => {
  const jmlItemUnsur = document.querySelector('.box-pesanan h3');
  const jumlahItem   = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}
